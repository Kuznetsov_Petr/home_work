def cook_book_file(my_file_path):
    with open(my_file_path, "r",  encoding='UTF8') as f:
      line = f.readline()
      cook_book = dict()
      while line:
        while not line.strip():
          line = f.readline()
        key = line.strip()
        ingridients_list = []
        ingridients_num = int(f.readline().strip())
        for _ in range(ingridients_num):
          dict_value = f.readline().strip().split("|")
          ingridients_dict = {
            "ingredient_name": dict_value[0].strip(),
            "quantity": int(dict_value[1]),
            "measure": dict_value[2].strip()
          }
          ingridients_list.append(ingridients_dict)
        cook_book[key] = ingridients_list
        line = f.readline()
    return cook_book

def get_shop_list_by_dishes(dishes, person_count,cook_book):
    shop_list = {}
    for dish in dishes:
        for ingridient in cook_book[dish]:
            new_shop_list_item = dict(ingridient)
            new_shop_list_item['quantity'] *= person_count
            if new_shop_list_item['ingredient_name'] not in shop_list:
                shop_list[new_shop_list_item['ingredient_name']] = new_shop_list_item
            else:
                shop_list[new_shop_list_item['ingredient_name']]['quantity'] += new_shop_list_item['quantity']
    return shop_list

def print_shop_list(shop_list):
    for shop_list_item in shop_list.values():
        print('{ingredient_name}: {quantity} {measure} '.format(**shop_list_item))


def create_shop_list():
    person_count = int(input('Введите количество людей: '))
    dishes = input("Введите блюда для одного человека: ").lower().split(',')
    cook_book = cook_book_file("The_name_of_the_dish.txt")
    shop_list = get_shop_list_by_dishes(dishes, person_count, cook_book)
    print_shop_list(shop_list)

create_shop_list()

# while True:
#     print("""
#     Введите p - по умолчанию The_name_of_the_dish.txt
#     Введите a - новый путь файла
#     Введите е - для выхода
#     """)
#     command = str(input("Введите команду: ")).lower()
#     if command == "p":
#         print("Стандартный путь файла The_name_of_the_dish.txt")
#         cook_book = cook_book_file("The_name_of_the_dish.txt")
#         create_shop_list()
#     if command == "a":
#         cook_book = cook_book_file(input("Название файла"))
#         create_shop_list()
#     if command == "e":
#         print("До скорого")
#         break
